import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author tkoleva on 09/22/16.
 */
public class Response {
    @Expose
    @SerializedName("carrier_logo")
    List<CarrierInfo> infos;

    @Expose
    @SerializedName("success")
    boolean success;

    public class CarrierInfo{
        @Expose
        @SerializedName("carrier_logo")
        String carrierLogo;

        @Expose
        @SerializedName("carrierName")
        String carrierName;

        @Expose
        @SerializedName("success")
        boolean success;
    }
}
