import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @author tkoleva on 09/20/16.
 */
public class APIProvider {

    private static Gson gson;
    private static API api;
    private static RequestInterceptor requestInterceptor;

    /**
     * Create api module api.
     *
     * @return the api
     * @throws IOException          the io exception
     * @throws NoSuchFieldException the no such field exception
     */
    public static API createApiModule() throws IOException, NoSuchFieldException {

        // Check if an API already exists
        if (api != null){
            return api;
        }

        requestInterceptor = new RequestInterceptor();

        // Creates a HTTP client
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .readTimeout(Constants.READ_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(Constants.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .build();

        Gson gson = getGson();

        // Builder for a request
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Constants.DEV)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson));

        api = builder.build().create(API.class);

        return api;
    }

    private static Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .setDateFormat(Constants.DATE_FORMAT)
                    .excludeFieldsWithoutExposeAnnotation()
                    .serializeNulls()
                    .setLenient()
                    .create();
        }
        return gson;
    }
}