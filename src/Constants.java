/**
 * @author tkoleva on 09/20/16.
 */
public class Constants {

    // Base URL constants
    public static final String API_KEY = "apiKey";
    public static final String DEV_API_KEY = "1c00439fb2e8b325dc7afa9ac708ccc3";
    public static final String DEVCI_API_KEY = "5c35cea49e9a543795947813352074f5";

    public static final String DEV = "http://osi-kaiko-dev.azurewebsites.net/";
//    public static final String DEV = "http://kaikoci_development.php.objectsystems.com";

    // getRates request params
    public static final String KAIKO_PNG = "kaiko.png";
    public static final String CARRIER_LOGO = "carrier_logo";
    public static final String CARRIER_NAME = "carrierName";
    public static final String PRICE = "price";
    public static final int PALLETS = 4;
    public static final int PRODUCT_CLASS = 150;
    public static final int WEIGHT = 43;
    public static final int DEFAULT = 0;

    // Result file properties
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH-mm-ssZ";
    public static final String USER_DIR = "user.dir";
    public static final String FILE_NAME = "_MissingLogos.txt";

    //Timeouts
    public static final int READ_TIMEOUT = 45;
    public static final int CONNECT_TIMEOUT = 15;
}
