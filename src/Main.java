import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import retrofit2.Response;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The type Main.
 *
 * @author tkoleva on 09/20/16.
 */
public class Main {

    private static Path file;
    private static BufferedWriter writer;

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws IOException          the io exception
     * @throws NoSuchFieldException the no such field exception
     */
    public static void main(String[] args) throws IOException, NoSuchFieldException {
        API api = APIProvider.createApiModule();

        // Creates a result file with the current date timestamp in the name
        Date today = new Date();
        Format formatter = new SimpleDateFormat(Constants.DATE_FORMAT);
        file = Paths.get(System.getProperty(Constants.USER_DIR) + "/" + formatter.format(today)
                + Constants.FILE_NAME);

        // Opens a write buffer
        writer =  Files.newBufferedWriter(file, Charset.defaultCharset(), StandardOpenOption.CREATE,
                StandardOpenOption.APPEND);

        // Starts the file with the following search params
        writeResultFile("Pallets: " + Constants.PALLETS + " Product class: " + Constants.PRODUCT_CLASS
                + " Weight: " + Constants.WEIGHT);

        // Takes an origin Zip code from OriginPostCode
        for (OriginPostCode oZip : OriginPostCode.values()) {
            System.out.print("oZip: " + oZip.getValue() + "\n");

            // Checks every Zip code from DestPostCode with one of the OriginPostCode codes
            for (DestPostCode dZip : DestPostCode.values()) {
                System.out.print("  dZip: " + dZip.getValue() + "\n");

                Response response = null;

                // If the request times out, the exception is recorded in the result file
                try {
                    response = api.getRates(
                            oZip.getValue(), dZip.getValue(),
                            Constants.PALLETS, Constants.PRODUCT_CLASS, Constants.WEIGHT,
                            Constants.DEFAULT, Constants.DEFAULT, Constants.DEFAULT, Constants.DEFAULT,
                            Constants.DEFAULT, Constants.DEFAULT, Constants.DEFAULT, Constants.DEFAULT,
                            Constants.DEFAULT, Constants.PRICE)
                            .execute();
                } catch (SocketTimeoutException e) {
                    writeResultFile(e.getMessage());
                }

                // Writes one origin and one destination zip code in the result file
                JsonArray array = (JsonArray) response.body();
                writeResultFile("Origin zip: " + oZip.getValue() + " Destination zip:  " + dZip.getValue());

                // Checks what is the format of the response
                checkOuterArray(array);
            }
        }
        writer.close();
    }

    // Checks what is the outer format of the response
    // It's possible to be an array of objects or array of arrays, or only one object, or only one array
    // Example responses:
    // array [4] -> 0 [7], 1 {0}, 2 [7], 3 [1]
    // array [4] -> 0 {1}, 1 {1}, 2 {1}, 3 [1]
    // array [1] -> 0 [1]
    // array [1] -> 0 {1}
    private static void checkOuterArray(JsonArray array) {
        for (int i = Constants.DEFAULT; i < array.size(); i++) {
            JsonArray innerArray;
            // Checks if some of the outer array elements are objects
            try {
                JsonObject object = array.get(i).getAsJsonObject();
                checkObject(object);

                // Checks if some of the outer array elements are arrays or if there is no response
            } catch (IllegalStateException | NullPointerException e) {
                try {
                    innerArray = array.get(i).getAsJsonArray();
                    checkInnerArray(innerArray);
                } catch (IllegalStateException e1) {
                    continue;
                }
            }
        }
    }

    // Checks all array elements of the outer array
    private static void checkInnerArray(JsonArray innerArray) {
        for (int j = Constants.DEFAULT; j < innerArray.size(); j++) {
            // Checks if some of the outer array elements are objects
            try {
                JsonObject object = innerArray.get(j).getAsJsonObject();
                checkObject(object);
                // Checks if some of the inner array elements are other arrays or if there is no response
            } catch (IllegalStateException | NullPointerException e) {
                j++;
            }
        }
    }

    // Checks for all objects in the response arrays for kaiko.png logo
    // If there is such logo for a Carrier, it prints the Carrier's Name in the result file
    private static void checkObject(JsonObject object) throws IllegalStateException, NullPointerException {
        String carrierLogo = object.get(Constants.CARRIER_LOGO).getAsString();
        System.out.println(carrierLogo);
        if (Constants.KAIKO_PNG.equalsIgnoreCase(carrierLogo)) {
            String carrierName = object.get(Constants.CARRIER_NAME).getAsString();
            System.out.println("Failed carriers " + carrierName);
            writeResultFile(carrierName);
        }
    }

    // Writes the Carrier's Name in the result file
    private static void writeResultFile(String carrierName){
        try {
            writer.write(carrierName);
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
