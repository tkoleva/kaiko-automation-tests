
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * @author tkoleva
 * The type Request interceptor.
 */
public class RequestInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();

        //Builds a request for dev environment
        HttpUrl url = request.url().newBuilder().addQueryParameter(Constants.API_KEY,
                Constants.DEV_API_KEY).build();

        //Builds a request for devci environment
//        HttpUrl url = request.url().newBuilder().addQueryParameter(Constants.API_KEY,
//              Constants.DEVCI_API_KEY).build();
        request = request.newBuilder().url(url).build();

        return chain.proceed(request);
    }

}
