/**
 * Created by tkoleva on 09/21/16.
 */
public enum OriginPostCode {
    ALASKA("99507"),
    ALABAMA("35021"),
    ARKANSAS("71923"),
    ARIZONA("85251"),
    CALIFORNIA("90025"),
    COLORADO("80020"),
    CONNECTICUT("06226"),
    DISTRICT_OF_COLUMBIA("20016"),
    DELAWARE("19710"),
    FLORIDA("32601"),
    GEORGIA("30097"),
    HAWAII("96703"),
    IOWA("50020"),
    IDAHO("83402"),
    ILLINOIS("60002");

    private String value;

    OriginPostCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}