/**
 * @author tkoleva on 09/21/16.
 */

public enum DestPostCode {
    WYOMING("82070"),
    WEST_VIRGINIA("25423"),
    WISCONSIN("53012"),
    WASHINGTON("98011"),
    VERMONT("05672"),
    VIRGINIA("20110"),
    UTAH("84092"),
    TEXAS("73301"),
    TENNESSEE("37040"),
    SOUTH_DAKOTA("57730"),
    SOUTH_CAROLINA("29303"),
    RHODE_ISLAND("02840"),
    PENNSYLVANIA("17201"),
    OREGON("97132"),
    OKLAHOMA("73003"),
    OHIO("43008");

    private String value;

    DestPostCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
