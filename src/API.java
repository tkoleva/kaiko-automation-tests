import com.google.gson.JsonArray;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * The interface Api.
 *
 * @author tkoleva on 09/20/16. Uses Retrofit library for constructing the request URL
 */
public interface API {

    /**
     * Gets rates.
     *
     * @param originZip           the origin zip
     * @param destinationZip      the destination zip
     * @param pallets             the pallets
     * @param productClass        the product class
     * @param weight              the weight
     * @param originResidential   the origin residential
     * @param originLiftgate      the origin liftgate
     * @param originHazmat        the origin hazmat
     * @param originLimitedAccess the origin limited access
     * @param destResidential     the dest residential
     * @param destLiftgate        the dest liftgate
     * @param destInsideDelivery  the dest inside delivery
     * @param destLimitedAccess   the dest limited access
     * @param destNotifyDelivery  the dest notify delivery
     * @param sortBy              the sort by
     * @return the rates
     */
    @GET("api/getRates")
    Call<JsonArray> getRates(@Query("o_zip") String originZip,
                             @Query("d_zip") String destinationZip,
                             @Query("p_pallets") int pallets,
                             @Query("p_class") int productClass,
                             @Query("p_weight") int weight,
                             @Query("o_residential") int originResidential,
                             @Query("o_liftgate") int originLiftgate,
                             @Query("o_hazmat") int originHazmat,
                             @Query("origin_limited_access") int originLimitedAccess,
                             @Query("d_residential") int destResidential,
                             @Query("d_liftgate") int destLiftgate,
                             @Query("d_inside_delivery") int destInsideDelivery,
                             @Query("d_limited_access") int destLimitedAccess,
                             @Query("d_notification_delivery") int destNotifyDelivery,
                             @Query("sortBy") String sortBy);

}
